#include "action.h"

#include <QSerialPortInfo>
#include <QDebug>
#include <QRegExp>
#include <cmath>
#include "serialdetectthread.h"

CAction::CAction(QObject *parent) : QObject(parent), m_serial(this)
{
	m_isOpen = false;
	m_pSerialDetectThread = new CSerialDetectThread(this);
	connect(m_pSerialDetectThread, SIGNAL(sigConnectST()), this, SLOT(slotOpenSerial()), Qt::QueuedConnection);
}

CAction::~CAction() {
	if (m_pSerialDetectThread) {
		if (m_pSerialDetectThread->isRunning()) {
			m_pSerialDetectThread->terminate();
			m_pSerialDetectThread->wait();

			delete m_pSerialDetectThread;
		}
	}
}

void CAction::slotOpenSerial() {
	/*
	Name:  "COM5"
	Desc:  "STMicroelectronics STLink Virtual COM Port"
	Port:  "066FFF514951775087051031"
	vendor:  "STMicroelectronics"
	*/

	if (m_isOpen) {
		qDebug("already opened!");
		return;
	}

	QSerialPortInfo serial_info;
	foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {
		if (info.manufacturer().compare("STMicroelectronics") == 0) {
			serial_info = info;
			break;
		}
	}

	if (serial_info.isNull()) {
		qDebug("ST serial port is not found!");
#if 0
		if (!m_pSerialDetectThread->isRunning()) {
			m_pSerialDetectThread->start();
		}
#endif
		return;
	}

	QThread::msleep(1000);
	m_serial.setPort(serial_info);
	if (!m_serial.open(QIODevice::ReadOnly)) {
		qDebug("open serial port %s failed!", serial_info.portName().toUtf8().data());
		return;
	}

	m_serial.setBaudRate(921600);
	m_serial.setParity(QSerialPort::NoParity);
	m_serial.setDataBits(QSerialPort::Data8);
	m_serial.setStopBits(QSerialPort::OneStop);
	m_serial.setFlowControl(QSerialPort::NoFlowControl);

	m_isOpen = true;
	m_serial.clearError();
	m_serial.clear();

	qDebug("connect to %s!", serial_info.portName().toUtf8().data());
	connect(&m_serial, SIGNAL(readyRead()), this, SLOT(slotReadPort()));
	connect(&m_serial, SIGNAL(errorOccurred(QSerialPort::SerialPortError)), this, SLOT(slotPortErrorAndWaitForReopen(QSerialPort::SerialPortError)));
}

void CAction::slotCloseSerial() {
	if (m_isOpen) {
		disconnect(&m_serial, SIGNAL(readyRead()), this, SLOT(slotReadPort()));
		disconnect(&m_serial, SIGNAL(errorOccurred(QSerialPort::SerialPortError)), this, SLOT(slotPortErrorAndWaitForReopen(QSerialPort::SerialPortError)));
		m_serial.close();
		m_isOpen = false;
#if 0
		if (!m_pSerialDetectThread->isRunning()) {
			qDebug("restart scan!");
			m_pSerialDetectThread->start();
		}
#endif
	}
}

void CAction::slotPortErrorAndWaitForReopen(QSerialPort::SerialPortError error) {
	qDebug("%s get error: %d", __PRETTY_FUNCTION__, error);
	switch(error) {
		case QSerialPort::ResourceError:
			slotCloseSerial();
			break;
		case QSerialPort::OpenError:
		default:
			break;
	}
}

void CAction::slotReopenSerial() {
	slotCloseSerial();
	slotOpenSerial();
}

void CAction::slotReadPort() {
	if (!m_isOpen) {
		qDebug("serial port is not opened");
		return;
	}

	QByteArray ar = m_serial.readAll();
	QString sz = ar;

	QStringList szs = sz.split('\n');

	for (int i = 0; i < szs.length() - 1; i++) {
		if (0 == readAngleInfo(szs.at(i))) {
			continue;
		}
		//else if (0 == readOrienInfo(szs.at(i))) {
		//	continue;
		//}
		else if (0 == readGyroInfo(szs.at(i))) {
			continue;
		}
	}
}

int CAction::readAngleInfo(QString szLog) {
	QRegExp exp("(DANG\\*)(\\w)\\((\\w+)\\) DEV_ORIENTATION\\((\\w+)\\) IS_PORTRAIT\\((\\w+)\\)");
	QVariantMap mapValue;
	if (exp.indexIn(szLog) != -1) {
		int offset = exp.cap(2).toInt();
		qlonglong angle = exp.cap(3).toLongLong();
		float fangle = static_cast<float>(angle) / static_cast<float>(pow(10, offset));
		mapValue["angle"] = (fangle / 360.0 * 100.0);
		int dev_orien = exp.cap(4).toInt();
		int is_port = exp.cap(5).toInt();
		switch (dev_orien) {
			default:
			case 0:
				mapValue["dev_orien"] = "y vertical point up";
				break;
			case 1:
				mapValue["dev_orien"] = "x vertical point up";
				break;
			case 2:
				mapValue["dev_orien"] = "y vertical point down";
				break;
			case 3:
				mapValue["dev_orien"] = "x vertical point down";
				break;
			case 4:
				mapValue["dev_orien"] = "face up";
				break;
			case 5:
				mapValue["dev_orien"] = "face down";
				break;
		}
		switch (is_port) {
			default:
			case 0:
				mapValue["is_port"] = "normal";
				break;
			case 1:
				mapValue["is_port"] = "portaint";
				break;
		}
		emit sigUpdateAngle(QVariant::fromValue(mapValue));
		return 0;
	}
	return 1;
}

int CAction::readOrienInfo(QString szLog) {
	QRegExp orienExp("(orien: )([-]?\\d+\\.\\d+), ([-]?\\d+\\.\\d+), ([-]?\\d+\\.\\d+)");
	if (orienExp.indexIn(szLog) != -1) {
		float x = orienExp.cap(2).toFloat();
		float y = orienExp.cap(3).toFloat();
		float z = orienExp.cap(4).toFloat();
		emit sigUpdateOrien(QVariant(x), QVariant(y), QVariant(z));
		return 0;
	}
	return 1;
}

int CAction::readGyroInfo(QString szLog) {
	QRegExp lineExp("(gyro: )([-]?\\d+\\.\\d+), ([-]?\\d+\\.\\d+), ([-]?\\d+\\.\\d+)");
	if (lineExp.indexIn(szLog) != -1) {
		//uint64_t time = static_cast<uint64_t>(lineExp.cap(5).toLongLong());
		static uint64_t time = 0;
		float y = lineExp.cap(4).toFloat();
		emit sigUpdateLineChart(QVariant(time++), QVariant(y));
		return 0;
	}
	return 1;
}

bool CAction::isSerialOpened() {
	return m_isOpen;
}
