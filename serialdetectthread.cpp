#include "serialdetectthread.h"
#include <QDebug>

CSerialDetectThread::CSerialDetectThread() : QThread()
{
	m_action = nullptr;
}

CSerialDetectThread::CSerialDetectThread(CAction *action) : QThread() {
	m_action = action;
	setTerminationEnabled(true);
}

CSerialDetectThread::~CSerialDetectThread() {

}

void CSerialDetectThread::run() {
	if (m_action == nullptr) {
		qDebug("no action pointer, return");
		return;
	}

	while (!m_action->isSerialOpened()) {
		QThread::msleep(500);
		emit sigConnectST();
	}
	qDebug("finished!");
}
