
set PROJ_DIR=%cd%
set OUT_DIR=%PROJ_DIR%\..\release
set PROJ_NAME=angle

echo clean last release
rd /s/q %OUT_DIR%
mkdir %OUT_DIR%

echo start qmake ...
qmake -o %OUT_DIR% -recursive %PROJ_NAME%.pro CONFIG+=release

if not %errorlevel% == 0 goto _BUILD_FAIL

cd %OUT_DIR%

echo start make ...
mingw32-make -j8

if not %errorlevel% == 0 goto _BUILD_FAIL

echo deploy windows version ...
cd release
del *.o moc* qrc*
windeployqt --qmldir %PROJ_DIR% %PROJ_NAME%.exe

if not %errorlevel% == 0 goto _BUILD_FAIL

echo "build success"
exit 0

:_BUILD_FAIL
echo "build failed"
exit 1