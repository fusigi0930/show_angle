import QtQuick 2.9
import QtQuick.Window 2.2
import QtCharts 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.5

Item {
	id: rootItem
	property alias pieValue: pieChart.value
	property alias otherValue: otherChart.value
	property alias valueDevOrien: valueDevOrien
	property alias valueisPort: valueisPort
	Rectangle {
		id: widgetAngle
		anchors.fill: parent

		ChartView {
			id: angleChart
			anchors.top: parent.top
			anchors.left: parent.left
			height: parent.height - 60
			width: parent.width
			antialiasing: true
			theme: ChartView.ChartThemeBrownSand

			PieSeries {
				PieSlice {id: pieChart; label: "Angle"; value: 50.0 }
				PieSlice {id: otherChart; label: ""; value: 50.0 }
			}

		}
		Text {
			id: labelDevOrien
			anchors.left: parent.left
			anchors.top: angleChart.bottom
			text: " device orientation: "
			font.pixelSize: 24
		}
		Text {
			id: valueDevOrien
			anchors.left: labelDevOrien.right
			anchors.top: labelDevOrien.top
			font.pixelSize: 24
		}
		Text {
			id: labelIsPort
			anchors.left: parent.left
			anchors.top: labelDevOrien.bottom
			text: " is portaint mode: "
			font.pixelSize: 24
		}
		Text {
			id: valueisPort
			anchors.left: labelIsPort.right
			anchors.top: labelIsPort.top
			font.pixelSize: 24
		}
	}
}
