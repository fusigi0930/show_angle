#ifndef CACTION_H
#define CACTION_H

#include <QObject>
#include <QSerialPort>
#include <QVariant>
#include <QThread>

class CAction : public QObject
{
	Q_OBJECT
private:
	QSerialPort m_serial;
	QThread *m_pSerialDetectThread;

	bool m_isOpen;
public:
	explicit CAction(QObject *parent = nullptr);
	virtual ~CAction();

private:
	int readAngleInfo(QString szLog);
	int readOrienInfo(QString szLog);
	int readGyroInfo(QString szLog);

signals:
	void sigUpdateAngle(QVariant value);
	void sigUpdateOrien(QVariant x, QVariant y, QVariant z);
	void sigUpdateLineChart(QVariant x, QVariant y);

private slots:
	void slotReadPort();

public slots:
	void slotOpenSerial();
	void slotCloseSerial();
	void slotReopenSerial();
	void slotPortErrorAndWaitForReopen(QSerialPort::SerialPortError error);

public:
	bool isSerialOpened();
};

#endif // CACTION_H
