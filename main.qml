import QtQuick 2.9
import QtQuick.Window 2.2
import QtCharts 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.5
import Action 1.0
import "qrc:/qml/"
import "qrc:/image/"

ApplicationWindow {
	visible: true
	width: 1280
	height: 800
	title: qsTr("Angle")

	signal sigDCAdd();
	signal sigDCRemove();

	objectName: "main_win"

	onSigDCAdd: {
		action.slotOpenSerial();
	}

	onSigDCRemove: {
		console.log("yse! remove in qml");
	}

	Component.onCompleted: {
		action.slotOpenSerial();
	}

	toolBar: ToolBar {
		height: 46
		RowLayout {
			InviButton {
				width: 42
				height: 42
				buttonText: "reopen"
				labelText: "reopen"
				iconSource: "/image/image/reload.png"
				onSigClicked: {
					action.slotCloseSerial();
					action.slotOpenSerial();
				}
			}
		}
	}

	DeltaAngle {
		id: widgetAngle
		anchors.top: parent.top
		anchors.left: parent.left
		height: parent.height / 2
		width: parent.width / 2
	}

    Rotate3D {
        id: widgetRotate
        anchors.top: parent.top
        anchors.left: widgetAngle.right
        height: parent.height / 2
        width: parent.width / 2
		visible: false
    }

	Rectangle {
		id: widgetInfo
		anchors.top: widgetAngle.bottom
		anchors.left: parent.left
		height: parent.height / 2
		width: parent.width / 2
		Text {
			id: valueInfo
			anchors.horizontalCenter: parent.horizontalCenter
			anchors.verticalCenter: parent.verticalCenter
			font.pixelSize: 90
			text: "Wait"
			font.family: "Impact"
			font.bold: true
			color: "#3030FF"
		}
	}

	SensorLineChart {
		id: widgetLineChart
		anchors.top: widgetInfo.top
		anchors.left: widgetInfo.right
		height: parent.height / 2
		width: parent.width / 2
	}

	Item {
		id: keyHandle
		focus: true
		enabled: true
		Keys.onPressed: {
			if (event.modifiers & Qt.ControlModifier && event.key === Qt.Key_C && event.modifiers & Qt.AltModifier) {
				console.log("close serial port");
				action.slotCloseSerial();
			}
			else if (event.modifiers & Qt.ControlModifier && event.key === Qt.Key_O && event.modifiers & Qt.AltModifier) {
				console.log("open serial port");
				action.slotOpenSerial();
			}
		}
	}

	Action {
		id: action

		onSigUpdateAngle: {
			var angle = value.angle;
			widgetAngle.pieValue = angle;
			widgetAngle.otherValue = 100 - angle;
			widgetAngle.valueDevOrien.text = value.dev_orien;
			widgetAngle.valueisPort.text = value.is_port;

			if (value.is_port === "portaint") {
				valueInfo.text = "Book";
			}
			else {
				var real_angle = value.angle / 100.0 * 360.0;
				if (real_angle >= 0.0 && real_angle < 30.0) {
					valueInfo.text = "Lid close";
				}
				else if (real_angle >= 30.0 && real_angle < 150.0) {
					valueInfo.text = "Clamshell";
				}
				else if (real_angle >= 150.0 && real_angle < 210.0) {
					valueInfo.text = "Flat";
				}
				else if (real_angle >= 210.0 && real_angle <= 360.0) {
					valueInfo.text = "Tent";
				}
				else {
					valueInfo.text = "O.O";
				}
			}
		}

		onSigUpdateOrien: {
            widgetRotate.sigUpdateAxis(x, y, z);
		}

		onSigUpdateLineChart: {
			widgetLineChart.append(x, y);
		}
	}
}
