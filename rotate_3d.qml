import QtQuick 2.9
import QtCanvas3D 1.1
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.5
import "qrc:/obj_3d.js" as GLCode

Item {
    signal sigUpdateAxis(var x, var y, var z);

	Rectangle {
		id: rectTexture
		visible: false
		border.width: 3
		border.color: "darkslategray"
		width: 50
		height: 50
		layer.enabled: true
		layer.smooth: true
		Rectangle {
			id: rectRed
			width: parent.width / 2
			height: parent.height
			anchors.left: parent.left
			color: "darkred"
		}
		Rectangle {
			id: rectBlue
			width: parent.width /2
			height: parent.height
			anchors.left: rectRed.right
			color: "darkblue"
		}
	}

	Rectangle {
		anchors.fill: parent

		Canvas3D {
			anchors.top: parent.top
			anchors.left: parent.left
			width: parent.width
			height: parent.height - 120
			id: obj3d
			property double xRotAnim: 0
			property double yRotAnim: 0
			property double zRotAnim: 0

			onInitializeGL: {
				GLCode.initializeGL(obj3d, rectTexture);
			}

			onPaintGL: {
				GLCode.paintGL(obj3d);
			}

			onResizeGL: {
				GLCode.resizeGL(obj3d);
			}
		}

		RowLayout {
			id: controlLayout
			spacing: 5
			x: 12
			y: parent.height - 100
			width: parent.width - (2 * x)
			height: 100
			visible: true
			//! [1]

			Label {
				id: xRotLabel
				Layout.alignment: Qt.AlignRight
				Layout.fillWidth: false
				text: "X-axis:"
				font.pixelSize: 24
			}
			Label {
				id: xValueLabel
				Layout.fillWidth: false
				font.pixelSize: 24
			}

			Label {
				id: yRotLabel
				Layout.alignment: Qt.AlignRight
				Layout.fillWidth: false
				text: "Y-axis:"
				font.pixelSize: 24
			}

			Label {
				id: yValueLabel
				Layout.fillWidth: false
				font.pixelSize: 24
			}

			Label {
				id: zRotLabel
				Layout.alignment: Qt.AlignRight
				Layout.fillWidth: false
				text: "Z-axis:"
				font.pixelSize: 24
			}

			Label {
				id: zValueLabel
				Layout.fillWidth: false
				font.pixelSize: 24
			}
		}
	}

    onSigUpdateAxis: {
        obj3d.xRotAnim = x;
        obj3d.yRotAnim = y;
        obj3d.zRotAnim = z;
        xValueLabel.text = x.toString().substr(0, 5);
        yValueLabel.text = y.toString().substr(0, 5);
        zValueLabel.text = z.toString().substr(0, 5);
    }
}
