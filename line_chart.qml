import QtQuick 2.9
import QtQuick.Window 2.2
import QtCharts 2.3
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.5

Item {
	id: rootItem

	Rectangle {
		id: widgetLineChart
		property var datas: []
		anchors.fill: parent

		Timer {
			repeat: true
			interval: 20
			running: true
			onTriggered: {
				lineChart.requestPaint();
			}
		}
		ChartView {
			anchors.fill: parent

			LineSeries {
				axisY: CategoryAxis {
					min: -10
					max: 10
				}
			}

			Canvas {
				id: lineChart
				anchors.leftMargin: 10
				anchors.rightMargin: 10
				anchors.topMargin: 20
				anchors.bottomMargin: 10
				anchors.left: parent.left
				anchors.top: parent.top
				width: parent.width - 20
				height: parent.height - 30

				visible: true

				onPaint: {
					var ctx = getContext("2d");
					var cw = width;
					var ch = height;
					var y0 = height / 2;
					var maxX = 200;
					var minX = 0
					var maxY = 10;
					var minY = -10;
					var dY = height / (maxY - minY);
					var dX = width / (maxX - minX);
					var curX = 0;
					var curY = y0;

					ctx.lineWidth = 2;
					ctx.strokeStyle = "#5050E0";
					ctx.clearRect(0, 0, cw, ch);
					ctx.beginPath();
					ctx.moveTo(curX, curY);
					for (var i=0; i<widgetLineChart.datas.length; i++) {
						curX += dX;
						curY = y0 - (dY * widgetLineChart.datas[i]);
						ctx.lineTo(curX, curY)
					}
					ctx.stroke();
				}
			}
		}
	}

	function append(x, y) {
		if (x > 200) {
			widgetLineChart.datas.shift();
		}
		widgetLineChart.datas.push(y);
	}
}
