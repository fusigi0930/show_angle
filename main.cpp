#include <QApplication>
#include <QQmlApplicationEngine>

#include "action.h"
#include <QSerialPortInfo>
#include <QDebug>
#include "nativeeventfilter.h"

int main(int argc, char *argv[])
{
	//QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

	CNativeEventFilter Filter;
	QApplication app(argc, argv);
	qmlRegisterType<CAction>("Action", 1, 0, "Action");

	app.installNativeEventFilter(&Filter);
	QQmlApplicationEngine engine;
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
	if (engine.rootObjects().isEmpty())
		return -1;

	QObject *win = engine.rootObjects().at(0);
	if (win) {
		engine.connect(&Filter, SIGNAL(sigDeviceChangeAdd()), win, SIGNAL(sigDCAdd()));
	}


	int ret = app.exec();
	return ret;
}
