PROJ_DIR=$(pwd)
OUT_DIR=${PROJ_DIR}/../release
PROJ_NAME=angle

echo "start qmake..."
qmake -o ${OUT_DIR} -recursive ${PROJ_NAME}.pro CONFIG+=release

if [ "$?" != "0" ]; then
	echo "qmake error"
	exit 1
fi

echo "start make..."
cd $OUT_DIR
make -j8

if [ "$?" != "0" ]; then
	echo "make error"
	exit 1
fi


cd release
rm *.o moc* qrc*

