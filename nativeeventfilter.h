#ifndef NATIVEEVENTFILTER_H
#define NATIVEEVENTFILTER_H

#include <QAbstractNativeEventFilter>
#include <QObject>

class CNativeEventFilter : public QObject, public QAbstractNativeEventFilter
{
	Q_OBJECT
public:
	CNativeEventFilter();
	virtual ~CNativeEventFilter();

signals:
	void sigDeviceChangeAdd();
	void sigDeviceChangeRemove();

public:
	virtual bool nativeEventFilter(const QByteArray &eventType, void *message, long *result);
};

#endif // NATIVEEVENTFILTER_H
