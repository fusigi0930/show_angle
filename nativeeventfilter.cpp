#include "nativeeventfilter.h"

#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#include <dbt.h>
#endif

#include <QDebug>

CNativeEventFilter::CNativeEventFilter() : QAbstractNativeEventFilter(), QObject()
{

}

CNativeEventFilter::~CNativeEventFilter() {

}

bool CNativeEventFilter::nativeEventFilter(const QByteArray &eventType, void *message, long *result) {
	Q_UNUSED(eventType);
	Q_UNUSED(message);
	Q_UNUSED(result);
#if defined(Q_OS_WIN32) || defined(Q_OS_WIN32)
	MSG *msg = reinterpret_cast<MSG*>(message);
	if (msg && msg->message == WM_DEVICECHANGE) {
		switch (msg->wParam) {
			case DBT_DEVICEARRIVAL:
				emit sigDeviceChangeAdd();
				break;
			case DBT_DEVICEREMOVECOMPLETE:
				emit sigDeviceChangeRemove();
				break;
		}
	}
#endif
	return false;

}
