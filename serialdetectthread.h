#ifndef SERIALDETECTTHREAD_H
#define SERIALDETECTTHREAD_H
#include <QThread>
#include "action.h"

class CSerialDetectThread : public QThread
{
	Q_OBJECT
private:
	CAction	*m_action;

public:
	CSerialDetectThread();
	CSerialDetectThread(CAction *action);

	virtual ~CSerialDetectThread();

protected:
	virtual void run();

Q_SIGNALS:
	void sigConnectST();
};

#endif // SERIALDETECTTHREAD_H
